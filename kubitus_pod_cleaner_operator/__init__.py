# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Kubitus Pod Cleaner Operator."""

__title__ = 'kubitus_pod_cleaner_operator'
__version__ = '1.1.0'  # noqa: WPS410
__author__ = 'Mathieu Parent'  # noqa: WPS410
__email__ = 'math.parent@gmail.com'
__license__ = 'Apache2'
__copyright__ = 'Copyright 2021 Mathieu Parent'
