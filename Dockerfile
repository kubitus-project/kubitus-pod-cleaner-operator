FROM python:3.12.1 AS builder

COPY requirements.txt requirements.txt

RUN pip install --user -r requirements.txt

FROM python:3.12.1-slim

RUN adduser --uid 1000 --disabled-password --gecos '' kubitus-pod-cleaner-operator

COPY --from=builder --chown=kubitus-pod-cleaner-operator:kubitus-pod-cleaner-operator /root/.local /home/kubitus-pod-cleaner-operator/.local
COPY kubitus_pod_cleaner_operator kubitus_pod_cleaner_operator

USER 1000

ENV PATH=/home/kubitus-pod-cleaner-operator/.local/bin:$PATH

ENTRYPOINT ["kopf", "run", "--all-namespaces", "--liveness=http://0.0.0.0:8080/healthz", "kubitus_pod_cleaner_operator/handlers.py"]
