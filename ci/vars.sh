image_repository="$CI_REGISTRY_IMAGE"
# 0.3.0a0
kpco_pyver="$(grep __version__ kubitus_pod_cleaner_operator/__init__.py  | cut -d"'" -f2)"
# v0.3.0-a0 (semver)
kpco_version="v$(echo "$kpco_pyver" | sed s/a/-a/)"

if [ -n "$CI_COMMIT_TAG" ]; then
    if [ "$CI_COMMIT_TAG" != "$kpco_version" ]; then
        echo "Version mismatch: $CI_COMMIT_TAG != $kpco_version"
        exit 1
    fi
    helm_channel=stable

    kaniko_destinations="--destination $image_repository:$kpco_version --destination $image_repository:latest"
else
    # 2021-09-09-11-06-48 (busybox has limited recognized TIME formats)
    commit_date="$(date -d "$(echo "$CI_COMMIT_TIMESTAMP" | sed 's/T/ /' | cut -c -19)" +%Y-%m-%d-%H-%M-%S)"
    # my/branch_name-v1.x
    commit_branch="${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME:-$CI_COMMIT_BRANCH}"
    # my-branch-name-v1-x
    commit_branch_semver="$(echo -n "$commit_branch" | tr -c '[:alnum:]'  -)"
    # v0.3.0-a0-2021-09-09-11-06-48-wipbranch-6ac742d
    kpco_version="$kpco_version-$commit_date-${commit_branch_semver}-$CI_COMMIT_SHORT_SHA"
    # my-branch-name-v1-x
    helm_channel="$commit_branch_semver"

    kaniko_destinations="--destination $image_repository:$kpco_version"
fi
