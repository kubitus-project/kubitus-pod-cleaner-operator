# Kubitus Pod Cleaner Operator

:warning: **Unmaintained projet** :warning:

:recycle: Project will be archived in 2024.

## Summary

This operator fix job pods stuck waiting for envoy container to stop
([istio/istio#11659](https://github.com/istio/istio/issues/11659)).

It watches for Pods, and request Envoy to stop when it's the only remaining
container running.

Based on [kopf](https://github.com/nolar/kopf).

## Installation

### Using Helm

```shell
helm repo add kubitus-pod-cleaner-operator https://gitlab.com/api/v4/projects/32151358/packages/helm/stable
helm install kubitus-pod-cleaner-operator kubitus-pod-cleaner-operator/kubitus-pod-cleaner-operator
```

More information [here](doc/helm-install.md).

### Using plain manifests

Installation:

```bash
kubectl create namespace pod-cleaner
kubectl apply -n pod-cleaner -f deploy/manifests.yaml
```
